\section{\MakeUppercase{Сверточные нейронные сети}}
С появлением больших объемов данных и больших вычислительных возможностей стали активно использоваться нейронные сети. Особую популярность получили сверточные нейронные сети, архитектура которых была предложена Яном Лекуном и нацелена на эффективное распознавание изображений. Свое название архитектура сети получила из-за наличия операции свёртки, суть которой в том, что каждый фрагмент изображения умножается на матрицу (ядро) свёртки поэлементно, а результат суммируется и записывается в аналогичную позицию выходного изображения. В архитектуру сети заложены априорные знания из предметной области компьютерного зрения: пиксель изображения сильнее связан с соседним (локальная корреляция) и объект на изображении может встретиться в любой части изображения. Особое внимание светрочные нейронные сети получили после конкурса ImageNet, который состоялся в октябре 2012 года и был посвящен классификации объектов на фотографиях. В конкурсе требовалось распознавание образов в 1000 категорий. Победитель данного конкурса -- Алекс Крижевский, используя сверточную нейронную сеть, значительно превзошел остальных участников. Успех применения сверточных нейронных сетей к классификации изображений привел к множеству попыток использовать данный метод к другим задачам.

Сверточная нейронная сеть - специальная архитектура искусственных нейронных сетей, предложенная Яном Лекуном и нацеленная на эффективное распознавание изображений, входит в состав технологий глубинного обучения. Использует некоторые особенности зрительной коры, в которой были открыты так называемые простые клетки, реагирующие на прямые линии под разными углами, и сложные клетки, реакция которых связана с активацией определённого набора простых клеток. Таким образом, идея свёрточных нейронных сетей заключается в чередовании свёрточных слоев (convolution layers) и субдискретизирующих слоев (subsampling layers или pooling layers, слоёв подвыборки). Пример архитектуры сети расположен на рисунке \ref{fig:cnn-architecture}. Структура сети — однонаправленная (без обратных связей), принципиально многослойная.

\begin{figure}[!htp]
  \centering
  \includegraphics[scale=0.8]{cnn-architecture.png}
  \caption{Архитектура сверточной нейронной сети}
  \label{fig:cnn-architecture}
\end{figure}

Далее будут рассмотрены типы слоев из которых сверточная сеть состоит \cite{cnn-cs}.

\subsection{Сверточный слой (convolutional layer)}

Слой свёртки -- это основной блок свёрточной нейронной сети. Слой свёртки включает в себя для каждого канала свой фильтр, ядро свёртки которого обрабатывает предыдущий слой по фрагментам (суммируя результаты матричного произведения для каждого фрагмента). Весовые коэффициенты ядра свёртки (небольшой матрицы) неизвестны и устанавливаются в процессе обучения.

Особенностью свёрточного слоя является сравнительно небольшое количество параметров, устанавливаемое при обучении. Так например, если исходное изображение имеет размерность $100 \times 100$ пикселей по трём каналам (это значит 30000 входных нейронов), а свёрточный слой использует фильтры c ядром $3 \times 3$ пикселя с выходом на 6 каналов, тогда в процессе обучения определяется только 9 весов ядра, однако по всем сочетаниям каналов, то есть $9 \times 3 \times 6 = 162$, в таком случае данный слой требует нахождения только 162 параметров, что существенно меньше количества искомых параметров полносвязной нейронной сети. На рисунке \ref{fig:convolution} представлена операция свертки в графическом виде.

\begin{figure}[!htp]
  \centering
  \includegraphics[scale=2.5]{convolution.png}
  \caption{Операция свертки}
  \label{fig:convolution}
\end{figure}

Стоит также отметить, что хотя сверточный слой сокращает количество параметров по сравнению с полносвязным слоем, он использует больше гиперпараметров — параметров, выбираемых до начала обучения. В частности, выбираются следующие гиперпараметры:

\begin{itemize}
  \item Глубина (depth) — сколько ядер и коэффициентов смещения будет задействовано в одном слое;
  \item Шаг (stride) — на сколько смещается ядро на каждом шаге при вычислении следующего пикселя результирующего изображения. Обычно его принимают равным 1, и чем больше его значение, тем меньше размер выходного изображения;
  \item Отступ (padding): заметим, что свертка любым ядром размерности более, чем 1х1 уменьшит размер выходного изображения. Так как в общем случае желательно сохранять размер исходного изображения, рисунок дополняется нулями по краям;
\end{itemize}

\subsection{Субдискретизирующий слой (pooling layer)}

Слой пулинга (подвыборки, субдискретизации) представляет собой нелинейное уплотнение карты признаков, при этом группа пикселей (обычно размера $2 \times 2$) уплотняется до одного пикселя, проходя нелинейное преобразование. Наиболее употребительна при этом функция максимума. Преобразования затрагивают непересекающиеся прямоугольники или квадраты, каждый из которых ужимается в один пиксель, при этом выбирается пиксель, имеющий максимальное значение. Операция пулинга позволяет существенно уменьшить пространственный объём изображения. Пулинг интерпретируется так. Если на предыдущей операции свёртки уже были выявлены некоторые признаки, то для дальнейшей обработки настолько подробное изображение уже не нужно, и оно уплотняется до менее подробного. К тому же фильтрация уже ненужных деталей помогает не переобучаться. Слой пулинга, как правило, вставляется после слоя свёртки перед слоем следующей свёртки. На рисунке \ref{fig:pooling} можно увидеть операцию пулинга с функцией максимума, фильтром $2 \times 2$ и шагом 2.

\begin{figure}[!htp]
  \centering
  \includegraphics[scale=1]{pooling.png}
  \caption{Операция пулинга}
  \label{fig:pooling}
\end{figure}

\subsection{Полносвзяный слой (full-connected)}

Самый простой и широко применимый слой в нейронной сети – полносвязный. Каждый нейрон в этом слое – персептрон с нелинейной функцией активации. На вход каждому из нейронов полносвязного слоя подаются выходы всех нейронов предыдущего слоя. Нейрон считает сумму своих входов, умноженных на веса, добавляет порог, и полученное значение пропускает через функцию активации. То, что получилось в итоге, подается на выход нейрона.

\begin{equation}
  Output = f \left( \sum_i x_i \cdot w_i + bias \right)
\end{equation}

где $x_i$ -- вход нейрона, $w_i$ -- вес, сопоставленный этому входу, $bias$ -- аддитивный обучаемый порог. 

Работа самой сети происходит «слева направо», от входного слоя к выходному – так перемещается сигнал.

После нескольких прохождений свёртки изображения и уплотнения с помощью пулинга система перестраивается от конкретной сетки пикселей с высоким разрешением к более абстрактным картам признаков, как правило на каждом следующем слое увеличивается число каналов и уменьшается размерность изображения в каждом канале. В конце концов остаётся большой набор каналов, хранящих небольшое число данных (даже один параметр), которые интерпретируются как самые абстрактные понятия, выявленные из исходного изображения.

Эти данные объединяются и передаются на обычную полносвязную нейронную сеть, которая тоже может состоять из нескольких слоёв. При этом полносвязные слои уже утрачивают пространственную структуру пикселей и обладают сравнительно небольшой размерностью (по отношению к количеству пикселей исходного изображения). Пример полносвязного слоя представлен на рисунке \ref{fig:fullconnected}.

\begin{figure}[!htp]
  \centering
  \includegraphics[scale=1]{fullconnected.png}
  \caption{Полносвязный слой}
  \label{fig:fullconnected}
\end{figure}

\clearpage
